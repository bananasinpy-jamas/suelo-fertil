from django.apps import AppConfig

class CapacitacionesConfig(AppConfig):
    name = 'capacitaciones'

class TalleresConfig(AppConfig):
    name = 'talleres'

class suelo_fertil_appCon(AppConfig):
    name= 'suelo_fertil_app'
    verbose_name = 'Suelo Fertil'
