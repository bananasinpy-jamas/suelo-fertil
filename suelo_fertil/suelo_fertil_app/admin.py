from django.contrib import admin
from suelo_fertil_app.models import Capacitacion, Taller, historia, Organizacion, ImagenCapacitacion, ImagenTaller, ImagenInicio, ImagenHistoria, emailForma, ArchivoCapacitacion, ArchivoTaller,Contacto,Redes_Sociales, ContenidoMundoYoto, MundoYotoNav, InfoSueloFertil, MundoYoto, Archivo_Aportacion
from . import models
from django.utils.html import format_html
# Register your models here.



admin.site.register(MundoYoto)
admin.site.register(Archivo_Aportacion)

class ContactoAdmin(admin.ModelAdmin):
    def Modificar(self, obj):
        return format_html('<a class="changelink" href="/admin/suelo_fertil_app/contacto/{}/change/">Modificar</a>', obj.id_contacto)

    list_display = ('__str__', 'Modificar')

admin.site.register(Contacto, ContactoAdmin)


class RedesSocialesAdmin(admin.ModelAdmin):
    def Modificar(self, obj):
        return format_html('<a class="changelink" href="/admin/suelo_fertil_app/redes_sociales/{}/change/">Modificar</a>', obj.id_socailMedia)

    list_display = ('__str__', 'Modificar')

admin.site.register(Redes_Sociales, RedesSocialesAdmin)


class OrganizacionAdmin(admin.ModelAdmin):
    def Modificar(self, obj):
        return format_html('<a class="changelink" href="/admin/suelo_fertil_app/organizacion/{}/change/">Modificar</a>', obj.id_organizacion)

    def Eliminar(self, obj):
        return format_html('<a class="deletelink" href="/admin/suelo_fertil_app/organizacion/{}/delete/">Eliminar</a>', obj.id_organizacion)

    list_display = ('__str__', 'Modificar', 'Eliminar')

admin.site.register(Organizacion, OrganizacionAdmin)


class ImagenInicioAdmin(admin.ModelAdmin):
    def Modificar(self, obj):
        return format_html('<a class="changelink" href="/admin/suelo_fertil_app/imageninicio/{}/change/">Modificar</a>', obj.id_img_Inicio)

    def Eliminar(self, obj):
        return format_html('<a class="deletelink" href="/admin/suelo_fertil_app/imageninicio/{}/delete/">Eliminar</a>', obj.id_img_Inicio)

    list_display = ('__str__', 'Modificar', 'Eliminar')

admin.site.register(ImagenInicio, ImagenInicioAdmin)

class InfoSueloFertilAdmin(admin.ModelAdmin):
    def Modificar(self, obj):
        return format_html('<a class="changelink" href="/admin/suelo_fertil_app/infosuelofertil/{}/change/">Modificar</a>', obj.id)

    list_display = ('__str__', 'Modificar')

admin.site.register(InfoSueloFertil, InfoSueloFertilAdmin)



class ImagenCapacitacionInLine(admin.StackedInline):
    model = ImagenCapacitacion
    extra = 1
class ArchivoCapacitacionInLine (admin.StackedInline):
    model = ArchivoCapacitacion
    extra = 1
class CapacitacionAdmin(admin.ModelAdmin):
    fieldsets = [
        ('Información Básica Capacitacion', {'fields':['titulo_capacitacion', 'contenido_capacitacion']}),
        ('Video Capacitación',{'fields':['id_video', 'descripcion_video']}),
    ]
    inlines = [ImagenCapacitacionInLine, ArchivoCapacitacionInLine]

    def Modificar(self, obj):
        return format_html('<a class="changelink" href="/admin/suelo_fertil_app/capacitacion/{}/change/">Modificar</a>', obj.id_capacitacion)

    def Eliminar(self, obj):
        return format_html('<a class="deletelink" href="/admin/suelo_fertil_app/capacitacion/{}/delete/">Eliminar</a>', obj.id_capacitacion)

    list_display = ('__str__', 'Modificar', 'Eliminar')

admin.site.register(Capacitacion, CapacitacionAdmin)


class ImagenHistoriaInLine(admin.StackedInline):
    model = ImagenHistoria
    extra = 1
class historiaAdmin(admin.ModelAdmin):
    fieldsets = [
        ('Información Básica historia', {'fields':['titulo_historia', 'contenido_historia']}),
        ('Video historia',{'fields':['id_video', 'descripcion_video']}),
    ]
    inlines = [ImagenHistoriaInLine]

    def Modificar(self, obj):
        return format_html('<a class="changelink" href="/admin/suelo_fertil_app/historia/{}/change/">Modificar</a>', obj.id_historia)

    def Eliminar(self, obj):
        return format_html('<a class="deletelink" href="/admin/suelo_fertil_app/historia/{}/delete/">Eliminar</a>', obj.id_historia)

    list_display = ('__str__', 'Modificar', 'Eliminar')

admin.site.register(historia, historiaAdmin)


class ImagenTallerInLine(admin.StackedInline):
    model = ImagenTaller
    extra = 1
class ArchivoTallerInLine (admin.StackedInline):
    model = ArchivoTaller
    extra = 1
class TallerAdmin(admin.ModelAdmin):
    fieldsets = [
        ('Información Básica Taller', {'fields':['titulo_taller', 'contenido_taller']}),
        ('Video Taller',{'fields':['id_video', 'descripcion_video']}),
    ]
    inlines = [ImagenTallerInLine, ArchivoTallerInLine]

    def Modificar(self, obj):
        return format_html('<a class="changelink" href="/admin/suelo_fertil_app/taller/{}/change/">Modificar</a>', obj.id_taller)

    def Eliminar(self, obj):
        return format_html('<a class="deletelink" href="/admin/suelo_fertil_app/taller/{}/delete/">Eliminar</a>', obj.id_taller)

    list_display = ('__str__', 'Modificar', 'Eliminar')

admin.site.register(Taller, TallerAdmin)


class ContenidoMundoYotoInLine(admin.StackedInline):
    model = ContenidoMundoYoto
    extra = 0
class MundoYotoAdmin(admin.ModelAdmin):
    fieldsets = [
        ('Pestaña', {'fields': ['Titulo']}),
    ]
    inlines = [ContenidoMundoYotoInLine]

    def Modificar(self, obj):
        return format_html('<a class="changelink" href="/admin/suelo_fertil_app/mundoyotonav/{}/change/">Modificar</a>', obj.id_nav)

    def Eliminar(self, obj):
        return format_html('<a class="deletelink" href="/admin/suelo_fertil_app/mundoyotonav/{}/delete/">Eliminar</a>', obj.id_nav)

    list_display = ('__str__', 'Modificar', 'Eliminar')

admin.site.register(MundoYotoNav, MundoYotoAdmin)


class emailFormaAdmin(admin.ModelAdmin):
    def Modificar(self, obj):
        return format_html('<a class="changelink" href="/admin/suelo_fertil_app/emailforma/{}/change/">Modificar</a>', obj.id_email)

    def Eliminar(self, obj):
        return format_html('<a class="deletelink" href="/admin/suelo_fertil_app/emailforma/{}/delete/">Eliminar</a>', obj.id_email)

    list_display = ('__str__', 'Modificar', 'Eliminar')

admin.site.register(emailForma, emailFormaAdmin)
