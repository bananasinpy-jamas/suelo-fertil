from django.test import TestCase, Client
from django.contrib.auth import authenticate
from django.contrib.auth.models import User, Permission, Group
from django.urls import reverse
from .models import Taller, ImagenTaller, Capacitacion, ImagenCapacitacion, MundoYotoNav, InfoSueloFertil

class TallerModelsTest(TestCase):
    def test_create_Taller_valid(self):
        titulo = "Prueba"
        contenido = "Contenido Prueba"
        testResult = Taller.objects.create(titulo_taller = titulo, contenido_taller = contenido)
        if testResult and testResult.titulo_taller == titulo and testResult.contenido_taller == contenido:
            print('Test create valid workshop: Passed!')
        else:
            print('Test create valid workshop: Failed!')
        self.assertTrue(testResult)

    def test_create_Taller_invalid(self):
        testResult = Taller.objects.create(titulo_taller = "", contenido_taller = "")
        if testResult and testResult.titulo_taller != "" and testResult.contenido_taller != "":
            print('Test create invalid workshop: Passed! (Test not supposed to pass)')
        else:
            print('Test create invalid workshop: Failed! (Test supposed to fail)')
        self.assertTrue(testResult)

    def test_no_Talleres(self):
        response = self.client.get(reverse('suelo_fertil_app:talleres'))
        string = "No hay talleres disponibles por el momento"
        self.assertEqual(response.status_code, 200)
        if response.status_code == 200:
            print('Test view with no workshops: Passed!')
        else:
            print('Test view with no workshops: Failed!')
        self.assertContains(response, string)

    def test_modify_Taller(self):
        titulo = "Prueba"
        contenido = "Contenido Prueba"
        nuevo_titulo = "Nuevo titulo"
        self.t1 = Taller.objects.create(titulo_taller = titulo, contenido_taller = contenido)
        self.t1.titulo_taller = nuevo_titulo
        self.t1.save()
        if self.t1.titulo_taller == nuevo_titulo:
            print('Test modify title of workshop: Passed!')
        else:
            print('Test modify title of workshop: Failed!')
        self.assertEqual(self.t1.titulo_taller, nuevo_titulo)

    def test_modify_Taller2(self):
        titulo = "Prueba"
        contenido = "Contenido Prueba"
        contenido_nuevo = "Nueva descripcion o contenido"
        self.t1 = Taller.objects.create(titulo_taller = titulo, contenido_taller = contenido)
        self.t1.contenido_taller = contenido_nuevo
        self.t1.save()
        if self.t1.contenido_taller == contenido_nuevo:
            print('Test modify content of workshop: Passed!')
        else:
            print('Test modify content of workshop: Failed!')
        self.assertEqual(self.t1.contenido_taller, contenido_nuevo)

    def test_delete_Taller(self):
        titulo = "Prueba"
        contenido = "Contenido Prueba"
        self.t1 = Taller.objects.create(titulo_taller = titulo, contenido_taller = contenido)
        testResult = self.t1.delete()
        if testResult:
            print('Test delete workshop: Passed!')
        else:
            print('Test delete workshop: Failed!')
        self.assertTrue(testResult)

    def test_add_video_Taller(self):
        titulo = "Prueba"
        contenido = "Contenido Prueba"
        video = "Q_fx5s5DAK0"
        descripcion_video = "Video de avioncitos"
        self.t1 = Taller.objects.create(titulo_taller = titulo, contenido_taller = contenido)
        self.t1.id_video = video
        self.t1.save()
        self.t1.descripcion_video = descripcion_video
        self.t1.save()
        if self.t1.id_video != "" and self.t1.descripcion_video != "":
            print('Test add video to taller: Passed!')
        else:
            print('Test add video to taller: Failed!')
        self.assertNotEqual(self.t1.id_video, "")
        self.assertNotEqual(self.t1.descripcion_video, "")

    def test_modify_video_Taller(self):
        titulo = "Prueba"
        contenido = "Contenido Prueba"
        video = "Q_fx5s5DAK0"
        descripcion_video = "Video de avioncitos"
        nuevo_video = "rsJlT53jU_4"
        nueva_descripcion = "Video speaker bixby"
        self.t1 = Taller.objects.create(titulo_taller = titulo, contenido_taller = contenido, id_video = video, descripcion_video = descripcion_video)
        self.assertNotEqual(self.t1.id_video, "")
        self.assertNotEqual(self.t1.descripcion_video, "")
        self.t1.id_video = nuevo_video
        self.t1.save()
        self.t1.descripcion_video = nueva_descripcion
        self.t1.save()
        if self.t1.id_video == nuevo_video and self.t1.descripcion_video == nueva_descripcion:
            print('Test modify video from taller: Passed!')
        else:
            print('Test modify video from taller: Failed!')
        self.assertNotEqual(self.t1.id_video, video)
        self.assertNotEqual(self.t1.descripcion_video, descripcion_video)
        self.assertEqual(self.t1.id_video, nuevo_video)
        self.assertEqual(self.t1.descripcion_video, nueva_descripcion)

    def test_delete_video_Taller(self):
        titulo = "Prueba"
        contenido = "Contenido Prueba"
        video = "Q_fx5s5DAK0"
        descripcion_video = "Video de avioncitos"
        self.t1 = Taller.objects.create(titulo_taller = titulo, contenido_taller = contenido, id_video = video, descripcion_video = descripcion_video)
        self.assertNotEqual(self.t1.id_video, "")
        self.assertNotEqual(self.t1.descripcion_video, "")
        self.t1.id_video=""
        self.t1.save()
        self.t1.descripcion_video=""
        self.t1.save()
        if self.t1.id_video == "" and self.t1.descripcion_video == "":
            print('Test delete video from taller: Passed!')
        else:
            print('Test delete video from taller: Failed!')
        self.assertNotEqual(self.t1.id_video, "Q_fx5s5DAK0")
        self.assertNotEqual(self.t1.descripcion_video, "Video de avioncitos")
        self.assertEqual(self.t1.id_video, "")
        self.assertEqual(self.t1.descripcion_video, "")

class UsersTests(TestCase, Client):
    def test_create_User_valid(self):
        nombre = "tester"
        contra = "rootroot"
        testResult = User.objects.create_user(username = nombre, password = contra)
        if testResult and testResult.username == nombre:
            print('Test create valid user: Passed!')
        else:
            print('Test create valid user: Failed!')
        self.assertTrue(testResult)

    def test_modify_User(self):
        nombre = "tester"
        contra = "rootroot"
        nueva_contra = "newpassword"
        self.testResult = User.objects.create_user(username = nombre, password = contra)
        self.testResult.password = nueva_contra
        self.testResult.save()
        if self.testResult.password == nueva_contra:
            print('Test modify user: Passed!')
        else:
            print('Test modify user: Failed!')
        self.assertEqual(self.testResult.password, nueva_contra)

    def test_modify_User_invalid(self):
        testResult = User.objects.create(username = "", password = "")
        if testResult.username != "" and testResult.password != "":
            print('Test create invalid user: Passed! (Test not supposed to pass)')
        else:
            print('Test create invalid user: Failed! (Test supposed to fail)')
        self.assertTrue(testResult)

    def test_delete_user(self):
        nombre = "tester"
        contra = "rootroot"
        self.t1 = User.objects.create_user(username = nombre, password = contra)
        result = self.t1.delete()
        if result:
            print('Test delete user: Passed!')
        else:
            print('Test delete user: Failed!')
        self.assertTrue(result)

class LogInTest(TestCase):
    def test_login_valid(self):
        nombre = "testuser"
        contra = 12345
        nueva_contra = "hello"
        self.c = Client()
        self.user = User.objects.create(username = nombre, password = contra, is_active=True, is_staff=True, is_superuser=True)
        self.user.set_password(nueva_contra)
        self.user.save()
        self.user = authenticate(username = nombre, password = nueva_contra)
        testResult = self.c.login(username = nombre, password = nueva_contra)
        if testResult:
            print('Test log in: Passed!')
        else:
            print('Test log in: Failed!')
        self.assertTrue(testResult)

    def test_login_invalid(self):
        self.c = Client()
        testResult = self.c.login(username='test', password='none')
        if testResult:
            print('Test invalid log in: Failed!')
        else:
            print('Test invalid log in: Passed!')
        self.assertFalse(testResult)

    def test_logout_valid(self):
        nombre = "testuser"
        contra = 12345
        nueva_contra = "hello"
        self.c = Client()
        self.user = User.objects.create(username = nombre, password = contra, is_active=True, is_staff=True, is_superuser=True)
        self.user.set_password(nueva_contra)
        self.user.save()
        self.user = authenticate(username = nombre, password = nueva_contra)
        self.c.login(username = nombre, password = contra)
        testResult = self.c.logout()
        if testResult:
            print('Test log out: Failed!')
        else:
            print('Test log out: Passed!')
        self.assertFalse(testResult)

    def test_change_password(self):
        nombre = "testuser"
        contra = 12345
        nueva_contra = "hello"
        self.c = Client()
        self.user = User.objects.create(username = nombre, password = contra, is_active=True, is_staff=True, is_superuser=True)
        self.user.set_password(nueva_contra)
        self.user.save()
        testResult = self.c.login(username = nombre, password = nueva_contra)
        if testResult:
            print('Test change password: Passed!')
        else:
            print('Test change password: Failed!')
        self.assertTrue(testResult)

class CapacitacionModelsTest(TestCase):
    def test_create_Capacitacion_valid(self):
        self.assertTrue(Capacitacion.objects.create(titulo_capacitacion="Prueba Capacitacion", contenido_capacitacion="Contenido Prueba Capacitacion"))

    def test_no_Capacitaciones(self):
        response = self.client.get(reverse('suelo_fertil_app:capacitaciones'))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "No hay capacitaciones disponibles")

    def test_modify_Capacitacion(self):
        self.c1 = Capacitacion.objects.create(titulo_capacitacion="Prueba capacitacion", contenido_capacitacion="Contenido Prueba Capacitacion")
        self.c1.titulo_capacitacion="Nuevo titulo Capacitacion"
        self.c1.save()
        self.assertEqual(self.c1.titulo_capacitacion, "Nuevo titulo Capacitacion")

    def test_modify_Capacitacion2(self):
        self.c1 = Capacitacion.objects.create(titulo_capacitacion="Prueba", contenido_capacitacion="Contenido Prueba Capacitacion")
        self.c1.contenido_capacitacion="Nueva descripcion o contenido de capacitacion"
        self.c1.save()
        self.assertEqual(self.c1.contenido_capacitacion, "Nueva descripcion o contenido de capacitacion")

    def test_delete_Capacitacion(self):
        self.c1 = Capacitacion.objects.create(titulo_capacitacion="Prueba Capacitacion", contenido_capacitacion="Contenido Prueba Capacitacion")
        self.assertTrue(self.c1.delete())

    def test_add_video_Capacitacion(self):
        self.c1 = Capacitacion.objects.create(titulo_capacitacion="Prueba Capacitacion", contenido_capacitacion="Contenido Prueba Capacitacion")
        self.c1.id_video = "Q_fx5s5DAK0"
        self.c1.save()
        self.c1.descripcion_video="Video de prueba"
        self.c1.save()
        self.assertNotEqual(self.c1.id_video, "")
        self.assertNotEqual(self.c1.descripcion_video, "")

    def test_modify_video_Capacitacion(self):
        self.c1 = Capacitacion.objects.create(titulo_capacitacion="Prueba Capacitacion", contenido_capacitacion="Contenido Prueba", id_video="Q_fx5s5DAK0", descripcion_video="Video de prueba")
        self.assertNotEqual(self.c1.id_video, "")
        self.assertNotEqual(self.c1.descripcion_video, "")
        self.c1.id_video="rsJlT53jU_4"
        self.c1.save()
        self.c1.descripcion_video="Video nuevo"
        self.c1.save()
        self.assertNotEqual(self.c1.id_video, "Q_fx5s5DAK0")
        self.assertNotEqual(self.c1.descripcion_video, "Video de prueba")
        self.assertEqual(self.c1.id_video, "rsJlT53jU_4")
        self.assertEqual(self.c1.descripcion_video, "Video nuevo")

    def test_delete_video_Capacitacion(self):
        self.c1 = Capacitacion.objects.create(titulo_capacitacion="Prueba Capacitacion", contenido_capacitacion="Contenido Prueba Capacitacion", id_video="Q_fx5s5DAK0", descripcion_video="Video de prueba")
        self.assertNotEqual(self.c1.id_video, "")
        self.assertNotEqual(self.c1.descripcion_video, "")
        self.c1.id_video=""
        self.c1.save()
        self.c1.descripcion_video=""
        self.c1.save()
        self.assertNotEqual(self.c1.id_video, "Q_fx5s5DAK0")
        self.assertNotEqual(self.c1.descripcion_video, "Video de prueba")
        self.assertEqual(self.c1.id_video, "")
        self.assertEqual(self.c1.descripcion_video, "")

class MundoYotoNavModelsTest(TestCase):

    def test_create_MundoYotoNav_valid(self):
        self.assertTrue(MundoYotoNav.objects.create(Titulo="Prueba Mundo Yoto"))

    def test_modify_MundoYotoNav(self):
        self.m1 = MundoYotoNav.objects.create(Titulo="Prueba Mundo Yoto")
        self.m1.titulo_MundoYotoNav="Nuevo titulo"
        self.m1.save()
        self.assertEqual(self.m1.titulo_MundoYotoNav, "Nuevo titulo")

    def test_delete_MundoYotoNav(self):
        self.m1 = MundoYotoNav.objects.create(Titulo="Prueba Mundo Yoto")
        self.assertTrue(self.m1.delete())

class InfoSueloFertilModelsTest(TestCase):
    def test_create_InfoSueloFertil_valid(self):
        self.assertTrue(InfoSueloFertil.objects.create(Quienes_Somos="Somos..", Misión="Nuestra mision es..", Visión="Nuestra vision es.."))

    def test_modify_InfoSueloFertil(self):
        self.i1 = InfoSueloFertil.objects.create(Quienes_Somos="Somos..", Misión="Nuestra mision es..", Visión="Nuestra vision es..")
        self.i1.quienes_InfoSueloFertil="Nuevo quienes"
        self.i1.save()
        self.assertEqual(self.i1.quienes_InfoSueloFertil, "Nuevo quienes")

    def test_modify_InfoSueloFertil1(self):
        self.i1 = InfoSueloFertil.objects.create(Quienes_Somos="Somos..", Misión="Nuestra mision es..", Visión="Nuestra vision es..")
        self.i1.mision_InfoSueloFertil="Nueva mision"
        self.i1.save()
        self.assertEqual(self.i1.mision_InfoSueloFertil, "Nueva mision")

    def test_modify_InfoSueloFertil2(self):
        self.i1 = InfoSueloFertil.objects.create(Quienes_Somos="Somos..", Misión="Nuestra mision es..", Visión="Nuestra vision es..")
        self.i1.vision_InfoSueloFertil="Nueva vision"
        self.i1.save()
        self.assertEqual(self.i1.vision_InfoSueloFertil, "Nueva vision")

    def test_delete_InfoSueloFertil(self):
        self.i1 = InfoSueloFertil.objects.create(Quienes_Somos="Somos..", Misión="Nuestra mision es..", Visión="Nuestra vision es..")
        self.assertTrue(self.i1.delete())
