from django.urls import path
from suelo_fertil_app import views

app_name = 'suelo_fertil_app'
urlpatterns = [
    path ('suelo_fertil_app',views.index,name='index'),
    path ('inicio/',views.inicio,name='inicio'),
    path ('mundoyoto/',views.mundoyoto,name='mundoyoto'),
    path ('capacitaciones/',views.capacitaciones,name='capacitaciones'),
    path ('talleres/',views.talleres,name='talleres'),
    path ('historias/',views.historias,name='historias'),
    path ('contactanos/',views.contactanos,name='contactanos'),
    path ('colabora/',views.colabora,name='colabora'),
    path ('vistas/',views.pieVisitas, name='vistas'),
    path ('googleTimeline/',views.googleTimeline, name='google'),

]
