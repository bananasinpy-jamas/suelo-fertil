from django.db import models
from django import forms

class Capacitacion(models.Model):
    id_capacitacion = models.AutoField(primary_key=True)
    titulo_capacitacion = models.CharField(max_length=50)
    contenido_capacitacion = models.TextField(max_length=500)
    date= models.DateTimeField(auto_now_add=True)
    id_video = models.CharField(max_length=300, null=True, blank=True)
    descripcion_video = models.CharField(max_length=300, default="Video Capacitación Suelo Fertil")
    class Meta:
        verbose_name = "Capacitación"
        verbose_name_plural = "Capacitaciones"
        default_permissions = ()
        permissions = (
            ("add_Capacitacion","Agregar Capacitación"),
            ("change_Capacitacion","Modificar Capacitación"),
            ("delete_Capacitacion","Eliminar Capacitación")
        )
    def __str__(self):
        return self.titulo_capacitacion
    def titulo_c(self):
        return self.titulo_capacitacion
    def contenido_c(self):
        return self.contenido_capacitacion
    def primera (self):
        return self.imagen.all()[0]
    def todas (self):
        return self.imagen.all()
    def todosarchivos (self):
        return self.archivo.all()
    def project_fotus(self):
       try:
           return ",".join(map(lambda c: c.id_capacitacion , c.titulo_capacitacion))
       except ValueError:
           return "Error:"
    def cap_archivu(self):
        try:
            return ",".join(map(lambda c: c.id_capacitacion , c.archivo_capacitacion, c.imagen))
        except ValueError:
            return "Error:"

class Taller(models.Model):
    id_taller = models.AutoField(primary_key=True)
    titulo_taller = models.CharField(max_length=50)
    contenido_taller = models.TextField(max_length=1500)
    date = models.DateTimeField(auto_now_add=True)
    id_video = models.CharField(max_length=300, null=True, blank=True)
    descripcion_video = models.CharField(max_length=300, default="Video Taller Suelo Fertil")
    class Meta:
        verbose_name = "Taller"
        verbose_name_plural = "Talleres"
        default_permissions = ()
        permissions = (
            ("add_taller","Agregar Taller"),
            ("change_taller","Modificar Taller"),
            ("delete_taller","Eliminar Taller")
        )
    def __str__(self):
        return self.titulo_taller
    def titulo_t(self):
        return self.titulo_taller
    def contenido_t(self):
        return self.contenido_taller
    def primera (self):
        return self.imagen.all()[0]
    def todas (self):
        return self.imagen.all()
    def todosarchivos (self):
        return self.archivo.all()
    def project_fotus(self):
       try:
           return ",".join(map(lambda t: t.id_taller , t.titulo_taller))
       except ValueError:
           return "Error:"
    def taller_archivo(self):
        try:
            return ",".join(map(lambda t: t.id_taller , t.archivo_taller, t.imagen))
        except ValueError:
            return "Error:"

class historia(models.Model):
    id_historia = models.AutoField(primary_key=True)
    titulo_historia = models.CharField(max_length=50)
    contenido_historia = models.TextField(max_length=1500)
    date = models.DateTimeField(auto_now_add=True)
    id_video = models.CharField(max_length=300, null=True, blank=True)
    descripcion_video = models.CharField(max_length=300, default="Video Historia Suelo Fertil")
    class Meta:
        verbose_name = "Historia"
        verbose_name_plural = "Historias"
        default_permissions = ()
        permissions = (
            ("add_Historias","Agregar Historias"),
            ("change_Historias","Modificar Historias"),
            ("delete_Historias","Eliminar Historias")
        )
    def __str__(self):
        return self.titulo_historia
    def titulo_h(self):
        return self.titulo_historia
    def contenido_c(self):
        return self.contenido_historia
    def primera (self):
        return self.imagen.all()[0]
    def todas (self):
        return self.imagen.all()
    def project_fotus(self):
       try:
           return ",".join(map(lambda c: c.id_historia , c.titulo_historia))
       except ValueError:
           return "Error:"

class Organizacion(models.Model):
    id_organizacion = models.AutoField(primary_key=True)
    nombre_organizacion = models.CharField(max_length=1000)
    img_organizacion = models.ImageField(upload_to='static/images/org')
    link_organizacion = models.URLField(max_length=2000)
    date= models.DateTimeField(auto_now_add=True)
    class Meta:
        verbose_name = "Organicación"
        verbose_name_plural = "Organizaciones"
        default_permissions = ()
        permissions = (
            ("add_Organizacion","Agregar Organizacion"),
            ("change_Organizacion","Modificar Organizacion"),
            ("delete_Organizacion","Eliminar Organizacion")
        )
    def __str__(self):
        return self.nombre_organizacion
    def nombre_o(self):
        return self.nombre_organizacion
    def project_img(self):
       try:
           return ",".join(map(lambda c: c.img_organizacion))
       except ValueError:
           return "Error:"
    def link_o(self):
        return self.link_organizacion

class ImagenCapacitacion(models.Model):
    titulo = models.CharField(max_length=50, default='Sin titulo')
    id_capacitacion = models.ForeignKey(Capacitacion, on_delete=models.CASCADE, related_name='imagen')
    foto = models.ImageField(upload_to='static/images/capacitacionesi')
    class Meta:
        default_permissions = ()
        permissions = (
            ("add_ImagenCapacitacion","Agregar Imagen de Capacitación"),
            ("change_ImagenCapacitacion","Modificar Imagen de Capacitación"),
            ("delete_ImagenCapacitacion","Eliminar Imagen de Capacitación")
        )
    def __str__(self):
        return self.titulo
    def project_fotos(self):
       try:
           return ",".join(map(lambda c: c.id_capacitacion, c.titulo, c.foto))
       except ValueError:
           return "Error:"

class ImagenHistoria(models.Model):
    titulo = models.CharField(max_length=50, default='Sin titulo')
    id_historia = models.ForeignKey(historia, on_delete=models.CASCADE, related_name='imagen')
    foto = models.ImageField(upload_to='static/images/historiasi')
    class Meta:
        default_permissions = ()
        permissions = (
            ("add_ImagenHistoria","Agregar Imagen de Historia"),
            ("change_ImagenHistoria","Modificar Imagen de Historia"),
            ("delete_ImagenHistoria","Eliminar Imagen de Historia")
        )
    def __str__(self):
        return self.titulo
    def project_fotos(self):
       try:
           return ",".join(map(lambda t: t.id_historia, t.titulo, t.foto))
       except ValueError:
           return "Error:"

class ImagenTaller(models.Model):
    titulo = models.CharField(max_length=100, default='Sin titulo')
    id_taller = models.ForeignKey(Taller, on_delete=models.CASCADE, related_name='imagen')
    foto = models.ImageField(upload_to='static/images/talleresi')
    class Meta:
        default_permissions = ()
        permissions = (
            ("add_ImagenTaller","Agregar Imagen de Taller"),
            ("change_ImagenTaller","Modificar Imagen de Taller"),
            ("delete_ImagenTaller","Eliminar Imagen de Taller")
        )
    def __str__(self):
        return self.titulo
    def project_fotos(self):
       try:
           return ",".join(map(lambda t: t.id_taller, t.titulo, t.foto))
       except ValueError:
           return "Error:"

class ImagenInicio(models.Model):
    id_img_Inicio = models.AutoField(primary_key=True)
    titulo = models.CharField(max_length=50, default='Sin titulo')
    descripcion = models.TextField(max_length=500)
    foto = models.ImageField(upload_to='static/images/carrusel')
    class Meta:
        verbose_name = "Imágenes Inicio"
        verbose_name_plural = "Imágenes Inicio"
        default_permissions = ()
        permissions = (
            ("add_ImagenInicio","Agregar Imagen de la Página Principal"),
            ("change_ImagenInicio","Modificar Imagen de la Página Principal"),
            ("delete_ImagenInicio","Eliminar Imagen de la Página Principal")
        )
    def __str__(self):
        return self.titulo
    def descrip(self):
        return self.descripcion
    def project_fotos(self):
       try:
           return ",".join(map(lambda c: c.foto))
       except ValueError:
           return "Error:"

class emailForma(models.Model):
    id_email = models.AutoField(primary_key=True)
    correo_electronico = models.EmailField(max_length=50)
    class Meta:
        verbose_name = "Emails de Contáctanos"
        verbose_name_plural = "Emails de Contáctanos"
        default_permissions = ()
        permissions = (
            ("add_emailForma","Agregar Email de Contáctanos"),
            ("change_emailForma","Modificar Email de Contáctanos"),
            ("delete_emailForma","Eliminar Email de Contáctanos")
        )
    def __str__(self):
        return self.correo_electronico

class Contacto(models.Model):
     id_contacto = models.AutoField(primary_key=True)
     direccion = models.CharField(max_length=200)
     telefono = models.BigIntegerField()
     correo = models.EmailField(max_length=50)
     class Meta:
        default_permissions = ()
        permissions = (
            ("add_Contacto","Agregar Forma de Contacto"),
            ("change_Contacto","Modificar Formas de Contacto"),
            ("delete_Contacto","Eliminar Forma de Contacto")
        )
     def __str__(self):
         return self.direccion
     def dir(self):
          return self.direccion
     def tel(self):
         return self.telefono
     def mail(self):
          return self.correo

class Redes_Sociales(models.Model):
     id_socailMedia = models.AutoField(primary_key=True)
     facebook_Link = models.URLField(max_length=1000)
     twitter_Link = models.URLField(max_length=1000)
     instagram_Link = models.URLField(max_length=1000)
     class Meta:
        verbose_name = "Redes Sociales"
        verbose_name_plural = "Redes Sociales"
        default_permissions = ()
        permissions = (
            ("add_Redes_Sociales","Agregar Red Social"),
            ("change_Redes_Sociales","Modificar Red Social"),
            ("delete_Redes_Sociales","Eliminar Red Social")
        )
     def __str__(self):
          return self.facebook_Link
     def facebook(self):
          return self.facebook_Link
     def twitter(self):
          return self.twitter_Link
     def instagram(self):
          return self.instagram_Link

class ArchivoCapacitacion(models.Model):
    id_capacitacion = models.ForeignKey(Capacitacion, on_delete=models.CASCADE, related_name='archivo')
    archivo_capacitacion = models.FileField(upload_to='static/documents/capacitacion', null=True, blank=True)
    imagen_choices =(
        ('WORD', 'WORD'),
        ('PDF', 'PDF'),
        ('PPTX', 'PPTX'),
        ('EXCEL', 'EXCEL'),
        ('OTRO', 'OTRO'),
    )
    imagen_archivo = models.CharField(max_length=5, choices=imagen_choices, default='PDF')

    class Meta:
        default_permissions = ()
        permissions = (
            ("add_ArchivoCapacitacion","Agregar Archivo de una Capacitación"),
            ("change_ArchivoCapacitacion","Modificar Archivo de una Capacitación"),
            ("delete_ArchivoCapacitacion","Eliminar Archivo de una Capacitación")
        )
    def cap_archivo(self):
       try:
           return ",".join(map(lambda c: c.id_capacitacion, c.archivo_capacitacion, c.imagen_archivo))
       except ValueError:
           return "Error:"

class ArchivoTaller(models.Model):
    id_taller = models.ForeignKey(Taller, on_delete=models.CASCADE, related_name='archivo')
    titulo = models.CharField(max_length=50, default='Archivo para Descargar')
    archivo_taller = models.FileField(upload_to='static/documents/taller', null=True, blank=True)
    imagen_choices =(
        ('WORD', 'WORD'),
        ('PDF', 'PDF'),
        ('PPTX', 'PPTX'),
        ('EXCEL', 'EXCEL'),
        ('OTRO', 'OTRO'),
    )
    imagen_archivo = models.CharField(max_length=5, choices=imagen_choices, default='PDF')

    class Meta:
        default_permissions = ()
        permissions = (
            ("add_ArchivoTaller","Agregar Archivo de un Taller"),
            ("change_ArchivoTaller","Modificar Archivo de un Taller"),
            ("delete_ArchivoTaller","Eliminar Archivo de un Taller")
        )
    def taller_archivo(self):
       try:
           return ",".join(map(lambda t: t.id_taller, t.titulo, t.archivo_taller, t.imagen_archivo))
       except ValueError:
           return "Error:"

class MundoYotoNav(models.Model):
    id_nav = models.AutoField(primary_key=True)
    Titulo = models.CharField(max_length=50)
    date = models.DateTimeField(auto_now_add=True)
    class Meta:
        verbose_name = "Pestaña Mundo Yoto"
        verbose_name_plural = "Lista de pestañas Mundo Yoto"
    def todosmundo (self):
        return self.mundo.all()
    def __str__(self):
        return self.Titulo
    def project_mundoyoto(self):
       try:
           return ",".join(map(lambda c: c.id_mundo , c.titulo_mundo))
       except ValueError:
           return "Error:"
        
class ContenidoMundoYoto(models.Model):
    id_nav = models.ForeignKey(MundoYotoNav, on_delete= models.CASCADE, related_name='mundo')
    Titulo = models.CharField(max_length=100)
    parrafo1 = models.TextField(max_length=1000)
    foto1 =models.ImageField(upload_to='static/images/mundoyoto', blank=True, null=True)
    parrafo2 = models.TextField(max_length=1000, blank=True, null=True)
    foto2 =models.ImageField(upload_to='static/images/mundoyoto', blank=True, null=True)
    video = models.CharField(max_length=100, blank=True, null=True)
    archivo = models.FileField(upload_to='static/documents/mundoyoto', blank=True, null=True)
    imagen_choices =(
        ('WORD', 'WORD'),
        ('PDF', 'PDF'),
        ('PPTX', 'PPTX'),
        ('EXCEL', 'EXCEL'),
        ('OTRO', 'OTRO'),
    )
    imagen_archivo = models.CharField(max_length=5, choices=imagen_choices, default='PDF')
    class Meta:
        verbose_name = "Contenido Mundo Yoto"
        verbose_name_plural = "Lista de Contenido Mundo Yoto"
    def contenido_mundo(self):
       try:
           return ",".join(map(lambda t: t.id_mundo, t.parrafo1, t.foto1, t.parrafo2, t.foto2))
       except ValueError:
           return "Error:"

class InfoSueloFertil(models.Model):
    id = models.AutoField(primary_key=True)
    Quienes_Somos = models.TextField(max_length=300)
    Misión = models.TextField(max_length=300)
    Visión = models.TextField(max_length=300)
    class Meta:
        verbose_name = "Informacion Suelo Fertil"
        verbose_name_plural = "Informacion Suelo Fertil"
    def __str__(self):
        return self.Quienes_Somos

class MundoYoto(models.Model):
    id = models.AutoField(primary_key=True)
    Mundo_Rojo_Desarrollo_Productivo = models.TextField(max_length=1000, default="Mundo Rojo - Desarrollo productivo")
    Images_Mundo_Rojo = models.ImageField(upload_to='static/images/mundos', blank=True, null=True)
    Mundo_Violeta_Desarrollo_Intrapersonal = models.TextField(max_length=1000, default="Mundo Violeta - Desarrollo intrapersonal")
    Images_Mundo_Violeta = models.ImageField(upload_to='static/images/mundos', blank=True, null=True)
    Mundo_Verde_Desarrollo_Sustentable = models.TextField(max_length=1000, default="Mundo Verde - Desarrollo sustentable")
    Images_Mundo_Verde = models.ImageField(upload_to='static/images/mundos', blank=True, null=True)
    Mundo_Blanco_Desarrollo_Artístico = models.TextField(max_length=1000, default="Mundo Blanco - Desarrollo artístico")
    Images_Mundo_Blanco = models.ImageField(upload_to='static/images/mundos', blank=True, null=True)
    Mundo_Rosa_Desarrollo_Interpersonal = models.TextField(max_length=1000, default="Mundo Rosa - Desarrollo interpersonal")
    Images_Mundo_Rosa = models.ImageField(upload_to='static/images/mundos', blank=True, null=True)
    Mundo_Amarillo_Desarrollo_Cognitivo = models.TextField(max_length=1000, default="Mundo Amarillo - Desarrollo cognitivo")
    Images_Mundo_Amarillo = models.ImageField(upload_to='static/images/mundos', blank=True, null=True)
    Mundo_Azul_Desarrollo_Físico = models.TextField(max_length=1000, default="Mundo Azul - Desarrollo físico")
    Images_Mundo_Azul = models.ImageField(upload_to='static/images/mundos', blank=True, null=True)
    Flor_Mística = models.TextField(max_length=1000, default="Flor mística")
    class Meta:
        verbose_name = "Información de los mundos - Mundo Yoto"
        verbose_name_plural = "Lista de información de los mundos - Mundo Yoto"
    def __str__(self):
        return self.Mundo_Rojo_Desarrollo_Productivo

class Archivo_Aportacion (models.Model):
    año = models.CharField(max_length=20)
    Archivo = models.FileField(upload_to='static/contribuciones', blank=True, null=True)
    class Meta:
        verbose_name = "Archivo de contribuciones"
        verbose_name_plural = "Archivo de contribuciones"
    def __str__(self):
        return self.año
