from __future__ import unicode_literals
from django.shortcuts import render
from django.http import HttpResponse
from . import forms
from django.core.mail import send_mail

from suelo_fertil_app.models import Capacitacion, Taller, historia, Organizacion, ImagenCapacitacion, ImagenTaller, ImagenInicio, ImagenHistoria, emailForma,Contacto,Redes_Sociales, MundoYotoNav, ContenidoMundoYoto, InfoSueloFertil, MundoYoto, Archivo_Aportacion
import plotly
import plotly.graph_objs as go


inicioCnt = 0
indexCnt = 0
mundoCnt = 0
capCnt = 0
tallCnt = 0
hisCnt = 0


def inicio(request):
    global inicioCnt
    inicioCnt += 1
    print(inicioCnt)
    my_dict = {'insert_me':"Index"}
    contacto = Contacto.objects.order_by('id_contacto')
    redes_Sociales = Contacto.objects.order_by('id_socailMedia')
    contacto_inf = Contacto.objects.all()[:1]
    redes_S = Redes_Sociales.objects.all()[:1]
    organizaciones = Organizacion.objects.order_by('date')
    myOrg1 = Organizacion.objects.all()[:3]
    myOrg2 = Organizacion.objects.all()[3:6]
    myOrg3 = Organizacion.objects.all()[6:9]

    my_dict = {'insert_organizacinoes':myOrg1, 'insert_organizacinoes2':myOrg2, 'insert_organizacinoes3':myOrg3,'info_Contacto':contacto_inf, 'redes_Sociales':redes_S}
    return render (request,'suelo_fertil_app/inicio.html', context=my_dict)

def index(request):
    global indexCnt
    indexCnt += 1
    print(indexCnt)
    organizaciones = Organizacion.objects.order_by('date')
    info = InfoSueloFertil.objects.all()[:1]
    imagenesP = ImagenInicio.objects.all()[0:1]
    imagenesC = ImagenInicio.objects.all()[1:4]
    archivo = Archivo_Aportacion.objects.all()[:1]
    myOrg1 = Organizacion.objects.all()[:3]
    myOrg2 = Organizacion.objects.all()[3:6]
    myOrg3 = Organizacion.objects.all()[6:9]
    my_dict = {'insert_me':"Inicio", 'insert_info':info, 'insert_archivo':archivo, 'insert_organizacinoes':myOrg1, 'insert_organizacinoes2':myOrg2, 'insert_organizacinoes3':myOrg3,'insertImagenP':imagenesP, 'insert_imgCarrusel':imagenesC}

    return render (request,'suelo_fertil_app/index.html', context=my_dict)

def mundoyoto(request):
    global mundoCnt
    mundoCnt += 1
    print(mundoCnt)

    organizaciones = Organizacion.objects.order_by('date')
    mundos = MundoYoto.objects.all()[:1]
    navs = MundoYotoNav.objects.order_by('date')
    myOrg1 = Organizacion.objects.all()[:3]
    myOrg2 = Organizacion.objects.all()[3:6]
    myOrg3 = Organizacion.objects.all()[6:9]
    my_dict = {'insert_me':"Mundo Yoto", 'insert_modals':mundos, 'insert_navs':navs, 'insert_organizacinoes':myOrg1, 'insert_organizacinoes2':myOrg2, 'insert_organizacinoes3':myOrg3}

    return render (request,'suelo_fertil_app/mundoyoto.html', context=my_dict)

def capacitaciones(request):
    global capCnt
    capCnt += 1
    print(capCnt)
    capacitaciones = Capacitacion.objects.order_by('-date')
    organizaciones = Organizacion.objects.order_by('date')
    imagenes = ImagenCapacitacion.objects.all()
    myOrg1 = Organizacion.objects.all()[:3]
    myOrg2 = Organizacion.objects.all()[3:6]
    myOrg3 = Organizacion.objects.all()[6:9]
    my_dict = {'insert_capacitaciones':capacitaciones,'insert_organizacinoes':myOrg1, 'insert_organizacinoes2':myOrg2, 'insert_organizacinoes3':myOrg3, 'insert_imagenes':imagenes}

    return render (request,'suelo_fertil_app/capacitaciones.html', context=my_dict)

def talleres(request):
    global tallCnt
    tallCnt += 1
    print(tallCnt)
    talleres = Taller.objects.order_by('-date')
    organizaciones = Organizacion.objects.order_by('date')
    imagenes = ImagenTaller.objects.all()
    myOrg1 = Organizacion.objects.all()[:3]
    myOrg2 = Organizacion.objects.all()[3:6]
    myOrg3 = Organizacion.objects.all()[6:9]
    my_dict = {'insert_talleres':talleres, 'insert_organizacinoes':myOrg1, 'insert_organizacinoes2':myOrg2, 'insert_organizacinoes3':myOrg3, 'insert_imagenes':imagenes}

    return render (request,'suelo_fertil_app/talleres.html', context=my_dict)

def historias(request):
    global hisCnt
    hisCnt += 1
    print(hisCnt)
    organizaciones = Organizacion.objects.order_by('date')
    myOrg1 = Organizacion.objects.all()[:3]
    myOrg2 = Organizacion.objects.all()[3:6]
    myOrg3 = Organizacion.objects.all()[6:9]
    historias = historia.objects.order_by('-date')
    imagenes = ImagenHistoria.objects.all()
    my_dict = {'insert_historias':historias, 'insert_organizacinoes':myOrg1, 'insert_organizacinoes2':myOrg2, 'insert_organizacinoes3':myOrg3}

    return render (request,'suelo_fertil_app/historias.html', context=my_dict)

def contactanos(request):
    form = forms.emailForm()
    organizaciones = Organizacion.objects.order_by('date')
    mail = emailForma.objects.all()
    myOrg1 = Organizacion.objects.all()[:3]
    myOrg2 = Organizacion.objects.all()[3:6]
    myOrg3 = Organizacion.objects.all()[6:9]
    if request.method == 'POST':
        form = forms.emailForm(request.POST)

        if form.is_valid():

            if mail:
                mensaje = "Nombre: " + form.cleaned_data['nombres'] + "\nAppelido: " + form.cleaned_data['apellidos'] + "\nEmail: " + form.cleaned_data['email'] + "\n" + form.cleaned_data['comentario']
                for m in mail:
                    print("Email Send was a SUCCESS")
                    print(m.nombre)
                    send_mail(form.cleaned_data['motivo'], mensaje, m.nombre,
                    [m.nombre], fail_silently=False)
                    my_dict = {'insert_me':"Index"}
                return render (request,'suelo_fertil_app/inicio.html', context=my_dict)

    return render (request,'suelo_fertil_app/contactanos.html', {'form':form})

def colabora(request):
    organizaciones = Organizacion.objects.order_by('date')
    myOrg1 = Organizacion.objects.all()[:3]
    myOrg2 = Organizacion.objects.all()[3:6]
    myOrg3 = Organizacion.objects.all()[6:9]
    my_dict = {'insert_me':"Colabora", 'insert_organizacinoes':myOrg1, 'insert_organizacinoes2':myOrg2, 'insert_organizacinoes3':myOrg3}

    return render (request,'suelo_fertil_app/colabora.html', context=my_dict)

def pieVisitas(request):
    global inicioCnt, indexCnt, mundoCnt, capCnt, tallCnt, hisCnt
    fig = {
          "data": [
            {
              "values" : [inicioCnt+indexCnt,mundoCnt,capCnt,tallCnt, hisCnt],
              "labels" : ['inicio','Mundo Yoto','Capacitaciones','Talleres', 'Historias'],
              "hoverinfo":"label+percent",
              "type": "pie",

            }],

         'layout': {'title': 'Visitas en Suelo Fertil',}
        }
    plotly.offline.plot(fig, filename='Vistas')
    my_dict = {'insert_me':"Index"}

    return render (request,'admin', context=my_dict)

def googleTimeline(request):
    capacitaciones = Capacitacion.objects.order_by('-date')
    my_dict = {'insert_capacitaciones':capacitaciones}

    return render (request, 'suelo_fertil_app/googleTimeline.html', context=my_dict)
