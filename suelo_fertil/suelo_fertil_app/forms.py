from django import forms
from django.core import validators

def buscabotsval(value):
    if len(value) > 0:
        raise forms.ValidationError("Bot Detectado")

class emailForm(forms.Form):
    nombres = forms.CharField(widget=forms.TextInput(

            attrs={
                'class': 'form-control',
                'placeholder': 'Nombres'
            }
    ))
    apellidos = forms.CharField(widget=forms.TextInput(
            attrs={
                'class': 'form-control',
                'placeholder': 'Apellidos'
            }
    ))
    email = forms.EmailField(widget=forms.TextInput(
            attrs={
                'class': 'form-control',
                'placeholder': 'nombre@ejemplo.com'
            }
    ))


    motivo = forms.CharField(widget=forms.TextInput(
            attrs={
                'class': 'form-control',
                'placeholder': 'razón por la cual nos quiere contactar'
            }
    ))
    comentario = forms.CharField(widget=forms.Textarea(
            attrs={
                'class': 'form-control',
                'rows':4

            }
    ))
    buscabots = forms.CharField(required=False, widget=forms.HiddenInput, validators = [buscabotsval])
